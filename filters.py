#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul  5 13:20:21 2019

@author: jairo
"""
import pandas as pd
import numpy as np
from tqdm import tqdm
import os
import librosa

from scipy.io import wavfile

def envelope(y, rate, threshold):
    y = pd.Series(y).apply(np.abs)
    # it will apply the function filter 10 times per second
    y_mean = y.rolling(window=int(rate/10), min_periods=1, center=True).mean()
    
    mask = [mean > threshold for mean in y_mean]
    return mask

def clean(df):
    if(len(os.listdir('clean'))) == 0:
        for f in tqdm(df['ID']):
            file_name = str(f) + '.wav'
            # downsampling
            signal, rate = librosa.load('Train2/' + file_name, sr=16000)
            mask = envelope(signal, rate, threshold=0.0005)
            wavfile.write('clean/' + file_name, rate=rate, data=signal[mask])