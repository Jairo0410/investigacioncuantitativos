#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul  5 14:09:37 2019

@author: jairo
"""
import librosa
import numpy as np
import filters
from python_speech_features import mfcc, logfbank
import utils
import matplotlib.pyplot as plt


signals = {}
fft = {}
fbank = {}
mfccs = {}

def calc_fft(y, rate):
    n = len(y)
    freq = np.fft.rfftfreq(n, d=1/rate)
    # Mean normalization dividing by 'n'
    Y = abs(np.fft.rfft(y)/n)
    
    return Y, freq

def plot(df):
    classes = list(np.unique(df['Class']))

    for c in classes:
    
        wav_file = df[df['Class'] == c].iloc[0,0]
        signal, rate = librosa.load('clean/' + str(wav_file) + '.wav', sr=44100)
        mask = filters.envelope(signal, rate, 0.0005)
        signal = signal[mask]
        signals[c] = signal
        fft[c] = calc_fft(signal, rate)
        
        # nfft = 44100/40
        fbank[c] = logfbank(signal[:rate], rate,  nfilt=26, nfft=1103).T
        
        # numcep half of nfilt
        mfccs[c] = mfcc(signal[:rate], rate, numcep=13, nfilt=26, nfft=1103).T
    
    utils.plot_signals(signals)
    plt.show()
    
    utils.plot_fft(fft)
    plt.show()
    
    utils.plot_mfccs(mfccs)
    plt.show()
    
def class_distribution(df):
    class_dist = df.groupby(['Class'])['length'].mean()
    
    fig, ax = plt.subplots()
    ax.set_title('Class Distribution', y=1.08)
    ax.pie(class_dist, labels=class_dist.index, autopct='%1.1f%%', shadow=False, startangle=90)
    ax.axis('equal')
    plt.show()