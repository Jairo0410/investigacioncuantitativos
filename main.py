import pandas as pd
import numpy as np
from scipy.io import wavfile
import librosa
from tqdm import tqdm
import numpy as np
from python_speech_features import mfcc, logfbank

from tensorflow.keras.layers import Conv2D, MaxPool2D, Flatten, LSTM
from tensorflow.keras.layers import Dropout, Dense, TimeDistributed
from tensorflow.keras.models import Sequential
from tensorflow.keras.utils import to_categorical
from sklearn.utils.class_weight import compute_class_weight

import utils
from config import Config

import matplotlib.pyplot as plt

df = pd.read_csv("filtered.csv")
df = df[['ID', 'Class']]
df.set_index('ID', inplace=True)

classes = list(np.unique(df['Class']))

for f in df.index:
    file_route = 'clean/' + str(f) + '.wav'
    rate, signal = wavfile.read(file_route)
    df.at[f, 'length'] = signal.shape[0]/rate
    

class_dist = df.groupby(['Class'])['length'].mean()
n_samples = 2 * int(df['length'].sum())
prob_dist = class_dist / class_dist.sum()
choices = np.random.choice(class_dist.index, p=prob_dist)
    

n_samples = 2 * int(df['length'].sum())

conf = Config(mode='conv')

def get_conv_model():
    model = Sequential()
    model.add(Conv2D(16, (3, 3), activation='relu',
                     strides=(1,1), padding='same',
                     input_shape=input_shape))
    model.add(Conv2D(32, (3, 3), activation='relu',
                     strides=(1,1), padding='same'))
    model.add(Conv2D(64, (3, 3), activation='relu',
                     strides=(1,1), padding='same'))
    model.add(Conv2D(128, (3, 3), activation='relu',
                     strides=(1,1), padding='same'))
    model.add(MaxPool2D((2, 2)))
    model.add(Dropout(0.5))
    model.add(Flatten())
    model.add(Dense(128, activation='relu'))
    model.add(Dense(64, activation='relu'))
    model.add(Dense(10, activation='softmax'))
    
    model.summary()
    model.compile(loss='categorical_crossentropy',
                  optimizer='adam',
                  metrics=['acc'])
    
    return model

def build_rand_feat():
    X = []
    y = []
    
    _min, _max = float('inf'), -float('inf')
    for _ in tqdm(range(n_samples)):
        rand_class = np.random.choice(class_dist.index, p=prob_dist)
        file = np.random.choice(df[df['Class'] == rand_class].index)
        
        rate, wav = wavfile.read('clean/'+ str(file) + '.wav')
        label = df.at[file, 'Class']
        
        rand_index = np.random.randint(0, wav.shape[0]-conf.step)
        sample = wav[rand_index:rand_index+conf.step]
        X_sample = mfcc(sample, rate, numcep=conf.nfeat, 
                        nfilt=conf.nfilt, nfft=conf.nfft).T
                        
        _min = min(np.amin(X_sample), _min)
        _max = max(np.amax(X_sample), _max)
        
        X.append(X_sample if conf.mode == 'conv' else X_sample.T)
        y.append(classes.index(label))
    X, y = np.array(X), np.array(y)
    X = (X - _min)/(_max - _min)
    
    if conf.mode == 'conv':
        X = X.reshape(X.shape[0], X.shape[1], X.shape[2], 1)
    elif conf.mode == 'time':
        X = X.reshape(X.shape[0], X.shape[1], X.shape[2])
    
    y = to_categorical(y, num_classes=10) # There are 10 types of urban sound
    
    return X, y

if conf.mode == 'conv':
    X, y = build_rand_feat()
    input_shape = (X.shape[1], X.shape[2], 1)
    model = get_conv_model()
    
class_weight = compute_class_weight('balanced',
                                    classes,
                                    df['Class'])

model.fit(X, y, epochs=10, batch_size=32, shuffle=True, 
          class_weight=class_weight)

